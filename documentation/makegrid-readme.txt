Makegrid generates an adaptive grid from the particles data in the
snapshots as output from sfrhist.  The size of the grid cells is set
according to several accuracy criteria.

Makegrid takes a configuration filename as its only argument.  The
configuration file consists of keyword-value pairs which are used to
control the behavior of the program.

The required keywords (with example values) are:

input_file	snapshot_000.fits
		The input snapshot file (output from sfrhist).
output_file	grid_000.fits
		The output file, containing the grid data.
grid_min        -100. -100 -100	/ [kpc]
grid_max	100. 100 100	/ [kpc]
grid_n		10 10 10	/ [kpc]
		These three keywords specify the dimensions of the
		base grid.  Snapshot information outside of this range
		will be discarded.
gas_tolerance	.8	/ [] sigma gas/average gas allowed
L_bol_tolerance	.8	/ [] sigma L/average L allowed
		These two specify the fractional tolerance below which
		a grid of sub cells will be unified into one larger
		cell.  The number is (standard deviation of quantity
		in all the top-level sub cells that would be
		incorporated into one cell)/(average quantity over
		same cells).
n_rays_estimated	10000000 / [] Est. # of rays for tolerance
		The fractional criterion above will keep refining
		cells that have very little gas or luminosity as long
		as the fractional non uniformity is large.  This is a
		waste of grid cells as for any finite number of rays,
		there is a certain threshold below which no rays will
		be emitted or scattered.  This keyword provides a
		guideline to the program for calculating that
		threshold.  (To avoid artifacts, it should be set to
		something like 10x larger than the actual number of
		rays that will be shot.) 
opacity		6e-8	/ [kpc^2/M_sun] for guessing scatterings
		In order to provide an estimate for how many rays will
		scatter in a cell, the program needs to know an
		opacity which translates gas density into optical depth.
size_factor	4
		This is a fudge factor which is used to specify that
		it is pointless to create grid cells smaller than this
		fudge factor*(radius of the smallest particle in the
		region), as there is no information on smaller scales.
max_level	11
		This keyword provides an upper limit on how many
		levels of refinement the grid can have.
work_chunk_levels	5
		This is a parameter specifying that the work chunks
		that are distributed among the threads should not
		contain more than this number of refinement levels.
		Omitting this parameter will cause horrible load
		unbalance since then the work chunks will be cells of
		the base grid which are often very large and will have
		the majority of the particles in a galaxy in one of them.
mcrx_data_dir	/u7/patrik/code/transfer/
		This specifies the directory containing the
		interpolator file for mapping the mass of a spherical
		particle into a cubic cell.  This file is in the same
		directory as the mcrx code in cvs.

Optional keywords are:
n_threads	16
		The number of threads over which the convolution loop
		calculation will be distributed.  The default value is 1.
use_counters	false
		Specifies whether counters should print to standard
		out.  This is nice if you are running interactively,
		but makes log files messy for batch jobs.  Default is
		true. 
use_hpm		true
		Specifies whether the HPM Toolkit should be used for
		performance analysis.  Default is false.
CCfits_verbose	true
		Specifies whether CCfits should be in verbose mode,
		which will print diagnostic messages if there's a
		problem.  However, it will also print messages that
		look suspicious but are normal, so the default is
		false.
dump_grid_structure
		This keyword specifies a filename to which the
		coordinates of the corners of all the grid cells will
		be written.  This can be used to generate an image of
		the grid.  (This file can be VERY large.)  Default is
		not to dump.		
		
The output file is a FITS file.  In order to propagate all the
necessary input information, it contains a copy of all the HDU's in
the input snapshot file except the "OBJECTi" HDU's.  The pHDU has the
following keywords set:
FILETYPE    "SNAPSHOT" 
	    The file contains a simulation snapshot.
DATATYPE    "GRID"
	    The data is on a grid (as opposed to a particle set).
GRIDCODE    "MAKEGRID"
	    Specifies the code which was used to generate the grid, as
	    well as the HDU in which the configuration for the grid
	    generation can be found.  Currently, only MAKEGRID is used
	    here.

HDU "MAKEGRID" (or whatever is a specified by GRIDCODE):
This HDU contains the keywords in the input configuration file.

HDU "GRIDSTRUCTURE":
This HDU contains the structure of the adaptive grid itself, but no
cell data.  It is a binary table containing one column "structure", a
Boolean column indicating whether or not a grid cell is subdivided.
(There is a provision for grids where each cell is subdivided into
different numbers of sub cells, in which case there will be another
column indicating the number of sub cells.  Currently, we don't make
grids like that and can't handle them.)  The HDU also contains the
following keywords:
LENGTHUNIT  string
	    The length unit in which the grid dimensions are given.
MINX/Y/Z    float
	    The lower end of the region the grid is covering.
MAXX/Y/Z    float
	    The upper end of the region the grid is covering.
NX/Y/Z	    integer
	    The number of cells in the base grid in the different
	    directions.  Note that currently, the grid cells MUST be
	    cubic, so the values have to be chosen accordingly.
SUBDIVTP    string
	    Indicates that type of grid subdivision used.  "NONE"
	    means the grid is not adaptive (the "structure" column is
	    then not present), "UNIFORM" means that a grid cell is
	    always subdivided into the same number of sub cells given
	    by the keywords below, and "VARIABLE" means that each cell
	    may be subdivided into different numbers of cells.  As
	    mentioned above, this is not used.
SUBDIVX/Y/Z integer
	    The number of sub cells into which a cell is subdivided,
	    in the different directions.

The traversal order used in the "structure" column is the same as that
used by the grid class iterators.  Currently, this is "C-style"
meaning the Z-direction has the smallest stride.  The traversal is
also "depth-first", i.e. as soon as a "T" is in the structure column,
the next row refers to the first sub cell of the cell that is refined.

HDU "GRIDDATA":
This HDU contains the cell data.  The ordering is the same as in the
GRIDSTRUCTURE HDU.  It is a binary table containing the following
columns:
   mass_gas	    double     The gas mass in the grid cell
   mass_metals	    double     The metal mass in the grid cell
   L_bol	    double     The bolometric luminosity in the cell
   SFR		    double     The star formation rate in the cell
   cell_volume	    double     The volume of the cell
   L_lambda	    n*float    The SED in the cell.  (May be log of values.)

The HDU also contains the following keywords:
L_bol_tot   float
	    The total bolometric luminosity in the grid. 
M_g_tot	    float
	    The total gas mass in the grid.
M_metals_tot float
	     The total mass of metals in the grid.
SFR_tot	    float
	    The total star formation rate in the grid.

The HDU may also contain the keyword "logflux", which if true
indicates that the values in the L_lambda column is actually the log10
of the actual values.  (The point of this is that the value will fit
in a float, whereas the actual luminosity may be a number outside the
float range.)
   
	    